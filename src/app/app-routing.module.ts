import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { SowComponent } from './components/sow/sow.component';
import { ContingentStaffingComponent } from './components/contingent-staffing/contingent-staffing.component';
import { DirectHireComponent } from './components/direct-hire/direct-hire.component';
import { NearshoreAndOffshoreStaffingComponent } from './components/nearshore-and-offshore-staffing/nearshore-and-offshore-staffing.component';
import { RecruitmentProcessOutsourcingComponent } from './components/recruitment-process-outsourcing/recruitment-process-outsourcing.component';
import { PayrollSolutionsComponent } from './components/payroll-solutions/payroll-solutions.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  {
    path: 'sow',
    component: SowComponent
  },
  {
    path: 'contingent-staffing',
    component: ContingentStaffingComponent
  },
  {
    path: 'direct-hire',
    component: DirectHireComponent
  },
  {
    path: 'near-shore-and-offshore-staffing',
    component: NearshoreAndOffshoreStaffingComponent
  },
  {
    path: 'recruitment-process-outsourcing',
    component: RecruitmentProcessOutsourcingComponent
  },
  {
    path: 'payroll-solutions',
    component: PayrollSolutionsComponent
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'other',
    loadChildren: () => import('./other/other.module').then(m => m.OtherModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

export const appRoutingExport = [
  HomeComponent,
  PageNotFoundComponent,
  ContactUsComponent,
  SowComponent,
  ContingentStaffingComponent,
  DirectHireComponent,
  NearshoreAndOffshoreStaffingComponent,
  RecruitmentProcessOutsourcingComponent,
  PayrollSolutionsComponent
];

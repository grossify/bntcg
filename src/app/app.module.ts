import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { OwlModule } from "ngx-owl-carousel";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule, appRoutingExport } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { FooterComponent } from "./components/footer/footer.component";
import { CollapseModule } from "ngx-bootstrap/collapse";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { LayoutModule } from "@angular/cdk/layout";
import { AccordionModule } from "ngx-bootstrap/accordion";

@NgModule({
  declarations: [
    AppComponent,
    appRoutingExport,
    NavbarComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OwlModule,
    BrowserAnimationsModule,
    LayoutModule,
    CollapseModule.forRoot(),
    AccordionModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

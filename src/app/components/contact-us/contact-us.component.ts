import { Component, OnInit } from '@angular/core';
import { UpdateMetaDataAndTitleService } from '../../services/update-meta-data-and-title.service';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  constructor(
    private metaAndTitle: UpdateMetaDataAndTitleService,
    private meta: Meta
  ) {
    this.meta.updateTag({ name: 'description', content: 'Contact Us Page' });
    this.meta.updateTag({ name: 'author', content: 'Ramesh Kumar' });
    this.meta.updateTag({ name: 'keywords', content: 'Contact' });
    this.metaAndTitle.updateTitle('Contact Us');
  }

  ngOnInit() {}
}

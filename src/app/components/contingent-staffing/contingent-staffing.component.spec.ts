import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContingentStaffingComponent } from './contingent-staffing.component';

describe('ContingentStaffingComponent', () => {
  let component: ContingentStaffingComponent;
  let fixture: ComponentFixture<ContingentStaffingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContingentStaffingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContingentStaffingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

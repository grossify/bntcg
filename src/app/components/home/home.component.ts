import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public images = [
    'http://www.net2source.com/wp-content/uploads/2019/06/agile-1-best-of-the-best-gold.png',
    'http://www.net2source.com/wp-content/uploads/2019/06/DBJ.png',
    'http://www.net2source.com/wp-content/uploads/2019/06/inc-5000.png',
    'http://www.net2source.com/wp-content/uploads/2019/06/SIA-40-under-40.png',
    'http://www.net2source.com/wp-content/uploads/2019/07/TBJ-1.png',
    'http://www.net2source.com/wp-content/uploads/2019/07/America-most-honored-professionals-150x150-1.png',
    'http://www.net2source.com/wp-content/uploads/2019/06/DBJ.png'
  ];

  public owlOptions = {
    dots: true,
    navigation: true,
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    nav: false,
    responsive: {
        300: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 4
        }
    }
  };

  constructor() { }

  ngOnInit() {
  }

}

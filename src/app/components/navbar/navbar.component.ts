import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  HostListener,
  ViewEncapsulation
} from '@angular/core';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [
    {
      provide: BsDropdownConfig,
      useValue: { isAnimated: true, autoClose: true }
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit, OnDestroy {
  isCollapsed = true;

  public isWidthLessThan992px: boolean;

  public menuTogle = false;
  private selectedElement: any;

  public mobileQuery: MediaQueryList;
  private mobileQueryListener: () => void;

  mobileQueryCall(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ): void {
    this.mobileQuery = media.matchMedia('(max-width: 992px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    if (typeof this.mobileQuery.addEventListener !== 'undefined') {
      this.mobileQuery.addEventListener('change', this.mobileQueryListener);
    } else {
      // support for old browsers
      this.mobileQuery.addListener(this.mobileQueryListener);
    }
    if (this.mobileQuery.matches) {
      this.isWidthLessThan992px = true;
    } else {
      this.isWidthLessThan992px = false;
    }
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    if (event.target.innerWidth > 992) {
      this.isWidthLessThan992px = false;
    } else {
      this.isWidthLessThan992px = true;
    }
  }

  public clickOnNavControl(): void {
    this.menuTogle = !this.menuTogle;
  }

  private setNevbar(): void {
    const navbar = document.getElementById('navbar');
    const sticky = navbar.offsetTop;

    window.onscroll = () => {
      // if (this.menuTogle) {
      //   sticky = 500;
      // }
      if (this.menuTogle) {
        return;
      }
      myFunction();
    };

    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add('sticky');
      } else {
        navbar.classList.remove('sticky');
      }
    }

    // const menu1 = document.getElementById('menu-item-15464');
  }

  // public yourFunc(el) {
  //   if (el === this.selectedElement) {
  //     el.classList.add('show-level_2');
  //     } else {
  //       el.classList.remove('show-level_2');
  //       this.selectedElement = el;
  //     }
  // }

  // public clickOnAbout(el): void {
  //   const menu1 = document.getElementById('aboutUsLavel2Menu');
  //   if (!this.selectedElement) {
  //     menu1.classList.add('show-level_2');
  //     this.selectedElement = el;
  //   }
  //   if (el !== this.selectedElement) {
  //     menu1.classList.add('show-level_2');
  //     this.selectedElement.classList.remove('show-level_2');
  //     this.selectedElement = el;
  //   }
  // }

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQueryCall(changeDetectorRef, media);
  }

  ngOnInit() {
    this.setNevbar();
  }

  ngOnDestroy(): void {
    if (typeof this.mobileQuery.removeEventListener !== 'undefined') {
      this.mobileQuery.removeEventListener('change', this.mobileQueryListener);
    } else {
      this.mobileQuery.removeListener(this.mobileQueryListener);
    }
  }
}

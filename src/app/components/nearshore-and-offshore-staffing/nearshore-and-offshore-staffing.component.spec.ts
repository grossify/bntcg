import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearshoreAndOffshoreStaffingComponent } from './nearshore-and-offshore-staffing.component';

describe('NearshoreAndOffshoreStaffingComponent', () => {
  let component: NearshoreAndOffshoreStaffingComponent;
  let fixture: ComponentFixture<NearshoreAndOffshoreStaffingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearshoreAndOffshoreStaffingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearshoreAndOffshoreStaffingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

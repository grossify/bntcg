import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollSolutionsComponent } from './payroll-solutions.component';

describe('PayrollSolutionsComponent', () => {
  let component: PayrollSolutionsComponent;
  let fixture: ComponentFixture<PayrollSolutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollSolutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollSolutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruitmentProcessOutsourcingComponent } from './recruitment-process-outsourcing.component';

describe('RecruitmentProcessOutsourcingComponent', () => {
  let component: RecruitmentProcessOutsourcingComponent;
  let fixture: ComponentFixture<RecruitmentProcessOutsourcingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecruitmentProcessOutsourcingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruitmentProcessOutsourcingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

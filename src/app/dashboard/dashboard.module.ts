import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import {
  DashboardRoutingModule,
  dashboardRoutingExport,
} from "./dashboard-routing.module";

@NgModule({
  declarations: [...dashboardRoutingExport],
  imports: [CommonModule, DashboardRoutingModule, FormsModule],
})
export class DashboardModule {}

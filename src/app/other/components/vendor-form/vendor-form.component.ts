import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidationErrors,
} from "@angular/forms";
import { MyErrorStateMatcher } from "./MyErrorStateMatcher";
// import { ToastrService } from 'ngx-toastr';
import { VenderFormData } from "../../classes/vender-form-data";

@Component({
  selector: "app-vendor-form",
  templateUrl: "./vendor-form.component.html",
  styleUrls: ["./vendor-form.component.scss"],
})
export class VendorFormComponent implements OnInit {
  public vendorForm = new FormGroup({
    vendorName: new FormControl("", [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(128),
      Validators.pattern("^[^ ]+$"),
    ]),
    primeryContactPersonName: new FormControl("", [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(128),
      Validators.pattern("^[^ ]+$"),
    ]),
    contactNumber: new FormControl("", [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(15),
      Validators.pattern("^[^ ]+$"),
    ]),
    areaOfExpertise: new FormControl("", [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(2048),
      Validators.pattern("^[^ ]+$"),
    ]),
    officeAddress: new FormControl("", [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(512),
      Validators.pattern("^[^ ]+$"),
    ]),
    // password: new FormControl('', [
    //   Validators.required,
    //   Validators.minLength(3),
    //   Validators.maxLength(128),
    //   Validators.pattern('^[^ ]+$')
    // ])
  });

  // public UserNameMatcher = new MyErrorStateMatcher();
  // public UserPasswordMatcher = new MyErrorStateMatcher();

  private showErrors(element: string, keyError: any): void {
    let text: string;
    switch (keyError) {
      case "required":
        text = `${element} is required!`;
        break;
      case "pattern":
        text = `${element} has spaces, No spacea are allowed!`;
        break;
      case "minlength":
        text = `${element} is too short!`;
        break;
      case "maxlength":
        text = `${element} is very long!`;
        break;
    }
    // this.toastr.error(text, `Error in ${element}!`, {
    //   timeOut: 5000,
    //   closeButton: true
    // });
  }

  private getFormValidationErrors() {
    Object.keys(this.vendorForm.controls).forEach((key) => {
      const controlErrors: ValidationErrors = this.vendorForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach((keyError) => {
          // console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          switch (key) {
            case "vendorName":
              this.showErrors("Vendor Name", keyError);
              break;
            case "primeryContactPersonName":
              this.showErrors("Primery Contact Person Name", keyError);
              break;
            case "contactNumber":
              this.showErrors("Contact Number", keyError);
              break;
            case "areaOfExpertise":
              this.showErrors("Area Of Expertise", keyError);
              break;
            case "officeAddress":
              this.showErrors("Office Address", keyError);
              break;
          }
        });
      }
    });
  }

  public onSubmitVendorDetails(value: any): boolean {
    console.log(value);
    if (!this.vendorForm.valid) {
      this.getFormValidationErrors();
      return false;
    }
    // this.toastr.info('Login is in progress please wait', 'Info', {
    //   timeOut: 3000,
    //   closeButton: true
    // });

    // this.store.dispatch(LOGIN({LoginInputData: value}));
  }

  constructor() {} // private toastr: ToastrService

  ngOnInit(): void {}
}

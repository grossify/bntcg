import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtherComponent } from './other.component';
import { VendorFormComponent } from './components/vendor-form/vendor-form.component';

const routes: Routes = [
  {
    path: '',
    component: OtherComponent
  },
  {
    path: 'vendor-form',
    component: VendorFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtherRoutingModule { }

export const otherRoutingExport = [
  OtherComponent,
  VendorFormComponent
];

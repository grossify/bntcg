import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { ToastrModule } from 'ngx-toastr';

import { OtherRoutingModule, otherRoutingExport } from './other-routing.module';


@NgModule({
  declarations: [
    ...otherRoutingExport
  ],
  imports: [
    CommonModule,
    OtherRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // ToastrModule
  ]
})
export class OtherModule { }
